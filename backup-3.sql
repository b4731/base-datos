-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: pelucacool
-- ------------------------------------------------------
-- Server version	5.7.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cliente` (
  `idCliente` int(11) NOT NULL AUTO_INCREMENT,
  `estado` int(11) NOT NULL DEFAULT '1',
  `personaId` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCliente`),
  UNIQUE KEY `REL_ae0ef781c5c5e8130db4e0f708` (`personaId`),
  CONSTRAINT `FK_ae0ef781c5c5e8130db4e0f708a` FOREIGN KEY (`personaId`) REFERENCES `persona` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,1,4),(2,1,6),(3,1,7);
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_reservacion`
--

DROP TABLE IF EXISTS `detalle_reservacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `detalle_reservacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad` int(11) NOT NULL DEFAULT '1',
  `monto` float NOT NULL,
  `estado` int(11) NOT NULL DEFAULT '1',
  `reservacionIdReservacion` int(11) DEFAULT NULL,
  `empleadoIdEmpleado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_5a3155b332471073d9f09b3ec2a` (`reservacionIdReservacion`),
  KEY `FK_46179f238775b567506ce914f8a` (`empleadoIdEmpleado`),
  CONSTRAINT `FK_46179f238775b567506ce914f8a` FOREIGN KEY (`empleadoIdEmpleado`) REFERENCES `empleado` (`idEmpleado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_5a3155b332471073d9f09b3ec2a` FOREIGN KEY (`reservacionIdReservacion`) REFERENCES `reservacion` (`idReservacion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_reservacion`
--

LOCK TABLES `detalle_reservacion` WRITE;
/*!40000 ALTER TABLE `detalle_reservacion` DISABLE KEYS */;
INSERT INTO `detalle_reservacion` VALUES (12,2,40,2,8,1),(13,1,20,2,9,1),(14,1,20,2,10,1),(15,1,40,2,10,1),(16,1,20,2,11,1),(17,1,30,2,11,1);
/*!40000 ALTER TABLE `detalle_reservacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `empleado` (
  `idEmpleado` int(11) NOT NULL AUTO_INCREMENT,
  `areaId` int(11) DEFAULT NULL,
  `tipoEmpleado` varchar(15) DEFAULT NULL,
  `personaId` int(11) DEFAULT NULL,
  `empresaId` int(11) DEFAULT NULL,
  PRIMARY KEY (`idEmpleado`),
  UNIQUE KEY `REL_9c18c0cc319cfbb220c4c884db` (`personaId`),
  KEY `FK_d7ca17aadfa6ac12abaa516aebb` (`empresaId`),
  CONSTRAINT `FK_9c18c0cc319cfbb220c4c884dbd` FOREIGN KEY (`personaId`) REFERENCES `persona` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_d7ca17aadfa6ac12abaa516aebb` FOREIGN KEY (`empresaId`) REFERENCES `empresa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
INSERT INTO `empleado` VALUES (1,NULL,'Contratado',3,1),(2,NULL,'Planilla',5,3);
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `razonSocial` varchar(255) NOT NULL,
  `ruc` varchar(255) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `telefono` varchar(255) NOT NULL,
  `correo` varchar(255) NOT NULL,
  `tipoNegocio` varchar(255) DEFAULT NULL,
  `propietarioId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_dc656dc24f6986afbfe84ebf13` (`ruc`),
  KEY `FK_13f7702a3df17a5093bb708774a` (`propietarioId`),
  CONSTRAINT `FK_13f7702a3df17a5093bb708774a` FOREIGN KEY (`propietarioId`) REFERENCES `propietario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` VALUES (1,'La Poderosa','15341653124','Av. Jesus de Nazareth 731, Trujillo','965301524','lapoderosa2020@gmail.com',NULL,1),(3,'Calvos Unidos','13542423511','Av. Diego de Almagro 458, Trujillo','962301415','calvosunidos2020@gmail.com',NULL,1),(4,'TuPelada CambiosBah','11345345514','Av. Puro De Corazao 542, Trujillo','954201441','tupelada2020@gmail.com',NULL,1);
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manuales`
--

DROP TABLE IF EXISTS `manuales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `manuales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `archivo` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manuales`
--

LOCK TABLES `manuales` WRITE;
/*!40000 ALTER TABLE `manuales` DISABLE KEYS */;
INSERT INTO `manuales` VALUES (5,'administrador',0,'uploads/manuales/JONATHAN DOMINGO AMARANTO TANDAYPAN.pdf'),(6,'usuario',0,'uploads/manuales/JONATHAN DOMINGO AMARANTO TANDAYPAN.pdf'),(7,'usuario',0,'uploads/manuales/JONATHAN DOMINGO AMARANTO TANDAYPAN.pdf'),(8,'usuario',0,'uploads/manuales/JONATHAN DOMINGO AMARANTO TANDAYPAN.pdf'),(9,'administrador',0,'uploads/manuales/JONATHAN DOMINGO AMARANTO TANDAYPAN.pdf'),(10,'usuario',0,'uploads/manuales/JONATHAN DOMINGO AMARANTO TANDAYPAN.pdf'),(11,'administrador',0,'uploads/manuales/JONATHAN DOMINGO AMARANTO TANDAYPAN.pdf'),(12,'administrador',0,'uploads/manuales/JONATHAN DOMINGO AMARANTO TANDAYPAN.pdf'),(13,'administrador',0,'uploads/manuales/JONATHAN DOMINGO AMARANTO TANDAYPAN.pdf'),(14,'administrador',0,'uploads/manuales/JONATHAN DOMINGO AMARANTO TANDAYPAN.pdf'),(15,'usuario',0,'uploads/manuales/JONATHAN DOMINGO AMARANTO TANDAYPAN.pdf'),(16,'usuario',0,'uploads/manuales/JONATHAN DOMINGO AMARANTO TANDAYPAN.pdf'),(18,'administrador',0,'uploads/manuales/JONATHAN DOMINGO AMARANTO TANDAYPAN.pdf'),(19,'usuario',0,'uploads/manuales/JONATHAN DOMINGO AMARANTO TANDAYPAN.pdf'),(20,'usuario',0,'uploads/manuales/JONATHAN DOMINGO AMARANTO TANDAYPAN.pdf'),(21,'administrador',0,'uploads/manuales/CAPTURAS DE PANTALLA INICIAL PARA VIDIRIERÍAS.docx'),(22,'administrador',0,'uploads/manuales/CAPTURAS DE PANTALLA INICIAL PARA VIDIRIERÍAS.docx'),(23,'usuario',0,'uploads/manuales/Detalle_de_Tarifario.xlsx'),(24,'usuario',0,'uploads/manuales/Captura de pantalla 2022-02-08 165123.png'),(25,'administrador',0,'uploads/manuales/wallpaperflare-cropped-9.jpg'),(26,'usuario',1,'uploads/manuales/naruto-wallpaper-62.jpg');
/*!40000 ALTER TABLE `manuales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu` (
  `idMenu` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `color` varchar(255) DEFAULT NULL,
  `icono` varchar(255) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idMenu`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'Maestros','rgba(41,41,41,.2)','fa fa-users',1);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pago`
--

DROP TABLE IF EXISTS `pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idTransaccion` varchar(255) NOT NULL,
  `monto` float NOT NULL,
  `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `reservacionIdReservacion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_8303787be036814cdf0bbe564f4` (`reservacionIdReservacion`),
  CONSTRAINT `FK_8303787be036814cdf0bbe564f4` FOREIGN KEY (`reservacionIdReservacion`) REFERENCES `reservacion` (`idReservacion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pago`
--

LOCK TABLES `pago` WRITE;
/*!40000 ALTER TABLE `pago` DISABLE KEYS */;
INSERT INTO `pago` VALUES (1,'1',20,'2022-01-26 22:15:37.558205',8),(2,'2',20,'2022-01-26 22:16:04.416996',9);
/*!40000 ALTER TABLE `pago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `persona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `fechaNacimiento` varchar(255) NOT NULL,
  `correo` varchar(255) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `celular` varchar(255) DEFAULT NULL,
  `dni` varchar(8) NOT NULL,
  `estado` int(11) NOT NULL DEFAULT '1',
  `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `archivo_dni` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_9be1c357217009199edce98d4d` (`dni`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (1,'Bruce rrr','Alcántara rrr','1997-03-23','balcantara97@gmail.com','Administrador','916475468','75079095',1,'2020-09-16 00:00:00.000000','2022-03-15 22:43:40.000000','uploads/manuales/26846.jpg|uploads/manuales/Captura de pantalla 2022-02-08 165410.png'),(2,'Ronald','Ruiz','1997-05-08T23:00:00.000Z','ronaldRO@gmail.com','Propietario','923145103','79563205',1,'2020-09-16 18:00:49.115719','2020-09-16 18:07:51.000000',NULL),(3,'Brandon','Alcántara','2000-03-02T23:25:00.000Z','brandonAC@gmail.com','Empleado','963413203','75079094',1,'2020-09-16 18:26:58.206910','2020-09-16 18:41:37.000000',NULL),(4,'Richard','Culquitante','1996-04-08T01:50:00.000Z','richardQC@gmail.com','Cliente','946132113','75613154',1,'2020-09-16 20:51:19.714000','2020-09-16 20:51:19.714000',NULL),(5,'Louis','Fortoure','1996-09-01T21:46:00.000Z','louisF@gmail.com','Empleado','984236841','75302147',1,'2020-09-27 16:47:26.575448','2020-09-27 16:47:26.575448',NULL),(6,'Jonathan','Amaranto','2022-01-03T03:55:00.000Z','jhtamaranto@gmail.com','Cliente','916074351','48501024',1,'2022-01-19 22:56:35.995000','2022-01-19 22:56:35.995000',NULL),(7,'ddd','dddd','2022-03-02T13:02:00.000Z','ddddd@dddddd.gmail.com','Cliente','111111111','12312311',1,'2022-03-16 08:03:09.646000','2022-03-16 08:14:22.000000','uploads/manuales/26846.jpg|uploads/manuales/Captura de pantalla 2022-02-08 165123.png|uploads/manuales/Captura de pantalla 2022-02-08 165410.png');
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `propietario`
--

DROP TABLE IF EXISTS `propietario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `propietario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipoComercial` varchar(255) DEFAULT NULL,
  `personaId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `REL_cca7307affde539821d9c627cd` (`personaId`),
  CONSTRAINT `FK_cca7307affde539821d9c627cda` FOREIGN KEY (`personaId`) REFERENCES `persona` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `propietario`
--

LOCK TABLES `propietario` WRITE;
/*!40000 ALTER TABLE `propietario` DISABLE KEYS */;
INSERT INTO `propietario` VALUES (1,NULL,2);
/*!40000 ALTER TABLE `propietario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publicidad`
--

DROP TABLE IF EXISTS `publicidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `publicidad` (
  `idPublicidad` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) DEFAULT NULL,
  `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`idPublicidad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publicidad`
--

LOCK TABLES `publicidad` WRITE;
/*!40000 ALTER TABLE `publicidad` DISABLE KEYS */;
/*!40000 ALTER TABLE `publicidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservacion`
--

DROP TABLE IF EXISTS `reservacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reservacion` (
  `idReservacion` int(11) NOT NULL AUTO_INCREMENT,
  `estado` int(11) NOT NULL DEFAULT '1',
  `fechaReservacion` varchar(255) DEFAULT NULL,
  `clienteIdCliente` int(11) DEFAULT NULL,
  PRIMARY KEY (`idReservacion`),
  KEY `FK_4f15ee798700d499473d3d204a2` (`clienteIdCliente`),
  CONSTRAINT `FK_4f15ee798700d499473d3d204a2` FOREIGN KEY (`clienteIdCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservacion`
--

LOCK TABLES `reservacion` WRITE;
/*!40000 ALTER TABLE `reservacion` DISABLE KEYS */;
INSERT INTO `reservacion` VALUES (5,2,'2020-10-02T05:00:00.000Z',1),(6,2,'2020-10-09T05:00:00.000Z',1),(7,2,'2020-10-15T05:00:00.000Z',1),(8,1,'2022-01-23T21:00:00.000Z',2),(9,1,'2022-01-24T19:05:05.000Z',2),(10,1,'2022-01-31T11:00:00.000Z',2),(11,1,'2022-01-27T11:00:00.000Z',2);
/*!40000 ALTER TABLE `reservacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservacion_servicio_empresa_servicio_empresa`
--

DROP TABLE IF EXISTS `reservacion_servicio_empresa_servicio_empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reservacion_servicio_empresa_servicio_empresa` (
  `reservacionIdReservacion` int(11) NOT NULL,
  `servicioEmpresaIdServicioEmpresa` int(11) NOT NULL,
  PRIMARY KEY (`reservacionIdReservacion`,`servicioEmpresaIdServicioEmpresa`),
  KEY `IDX_856ccb3f3873d0d6b62255edd8` (`reservacionIdReservacion`),
  KEY `IDX_ad44756feb9a11efe7ddd066d7` (`servicioEmpresaIdServicioEmpresa`),
  CONSTRAINT `FK_856ccb3f3873d0d6b62255edd8a` FOREIGN KEY (`reservacionIdReservacion`) REFERENCES `reservacion` (`idReservacion`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_ad44756feb9a11efe7ddd066d72` FOREIGN KEY (`servicioEmpresaIdServicioEmpresa`) REFERENCES `servicio_empresa` (`idServicioEmpresa`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservacion_servicio_empresa_servicio_empresa`
--

LOCK TABLES `reservacion_servicio_empresa_servicio_empresa` WRITE;
/*!40000 ALTER TABLE `reservacion_servicio_empresa_servicio_empresa` DISABLE KEYS */;
INSERT INTO `reservacion_servicio_empresa_servicio_empresa` VALUES (5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(10,4),(11,1),(11,3);
/*!40000 ALTER TABLE `reservacion_servicio_empresa_servicio_empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rol` (
  `idRol` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `codigo` varchar(255) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idRol`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES (1,'Administrador','administrador',1),(2,'Cliente','cliente',1),(3,'Empleado','empleado',1);
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol_submenu`
--

DROP TABLE IF EXISTS `rol_submenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rol_submenu` (
  `idRolSubmenu` int(11) NOT NULL AUTO_INCREMENT,
  `estado` int(11) NOT NULL DEFAULT '1',
  `rolIdRol` int(11) DEFAULT NULL,
  `rolsubmenuIdSubmenu` int(11) DEFAULT NULL,
  PRIMARY KEY (`idRolSubmenu`),
  KEY `FK_e33791b8d4b783df39143df16f2` (`rolIdRol`),
  KEY `FK_d177ebdc2202fac07f5ab3ac201` (`rolsubmenuIdSubmenu`),
  CONSTRAINT `FK_d177ebdc2202fac07f5ab3ac201` FOREIGN KEY (`rolsubmenuIdSubmenu`) REFERENCES `submenu` (`idSubmenu`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_e33791b8d4b783df39143df16f2` FOREIGN KEY (`rolIdRol`) REFERENCES `rol` (`idRol`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol_submenu`
--

LOCK TABLES `rol_submenu` WRITE;
/*!40000 ALTER TABLE `rol_submenu` DISABLE KEYS */;
INSERT INTO `rol_submenu` VALUES (1,1,1,1);
/*!40000 ALTER TABLE `rol_submenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicio`
--

DROP TABLE IF EXISTS `servicio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `servicio` (
  `idServicio` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT '1',
  `descripcion` varchar(255) DEFAULT NULL,
  `tiempo` float DEFAULT NULL,
  `tipoSexo` varchar(255) DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `lugarServicio` varchar(150) DEFAULT '1',
  `direccion` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idServicio`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicio`
--

LOCK TABLES `servicio` WRITE;
/*!40000 ALTER TABLE `servicio` DISABLE KEYS */;
INSERT INTO `servicio` VALUES (1,'Corte de Pelo',1,'cortes de pelos',1,'1',NULL,'2020-09-16 18:10:55.410592','1','1'),(2,'Corte Pelo Con degradado',1,'corte para caballero',1,'1',NULL,'2022-01-22 19:40:57.071081','1','1'),(3,'corte militar caballeros',1,'corte para caballero',1,'1',NULL,'2022-01-22 19:41:21.340004','1','1'),(4,'corte con degradado 2',1,'para caballeros',1,'1',NULL,'2022-01-22 19:41:53.927194','1','1'),(5,'ddd',1,'dddd',10,'1',NULL,'2022-03-20 11:52:21.070267','1','1'),(6,'dddd',1,'dddddddd',10,'3',NULL,'2022-03-20 11:54:29.199930','2','actualizada'),(7,'nombre',1,'descripcion',10,'1',NULL,'2022-03-20 11:59:01.475362','2','mi direccion'),(8,'ddd',1,'ddd',10,'1',NULL,'2022-03-20 12:00:53.594486','2','ddd'),(9,'ddd',1,'ddd',2,'1',NULL,'2022-03-20 12:30:14.720617','1','');
/*!40000 ALTER TABLE `servicio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicio_empresa`
--

DROP TABLE IF EXISTS `servicio_empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `servicio_empresa` (
  `idServicioEmpresa` int(11) NOT NULL AUTO_INCREMENT,
  `estado` int(11) NOT NULL DEFAULT '1',
  `tiempo` float DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `imagenServicio` varchar(255) DEFAULT NULL,
  `empresaId` int(11) DEFAULT NULL,
  `servicioIdServicio` int(11) DEFAULT NULL,
  PRIMARY KEY (`idServicioEmpresa`),
  KEY `FK_d4f9aff93f130c9981b77593a36` (`empresaId`),
  KEY `FK_fe08286b64d1506dd41b3eac03b` (`servicioIdServicio`),
  CONSTRAINT `FK_d4f9aff93f130c9981b77593a36` FOREIGN KEY (`empresaId`) REFERENCES `empresa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_fe08286b64d1506dd41b3eac03b` FOREIGN KEY (`servicioIdServicio`) REFERENCES `servicio` (`idServicio`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicio_empresa`
--

LOCK TABLES `servicio_empresa` WRITE;
/*!40000 ALTER TABLE `servicio_empresa` DISABLE KEYS */;
INSERT INTO `servicio_empresa` VALUES (1,1,3,'Corte moderno',20,'uploads/catalogo/fc9adc5ea37f830508449fde78eccea7.jpg',1,1),(2,1,1.5,'CORTE DEGRADADO',25,'uploads/catalogo/foto-pelo-corto-hombre.png',1,1),(3,1,2,'',30,'uploads/catalogo/1414d239def62353f15fede0770effe5.jpg',1,1),(4,1,2,'',40,'uploads/catalogo/cortes-pelo-verano-gq5.jpg',1,1),(5,1,1,'ASD',30,'uploads/catalogo/foto-pelo-corto-hombre.png',4,1),(6,1,1.5,'ddd',20,'uploads/catalogo/1414d239def62353f15fede0770effe5.jpg',4,1),(7,1,5,'',12,'uploads/catalogo/foto-pelo-corto-hombre.png',3,1),(8,1,3,'',50,'uploads/catalogo/1414d239def62353f15fede0770effe5.jpg',3,1),(9,1,1,'ddd',20,'uploads/catalogo/Captura de pantalla 2022-02-08 165123.png',1,2),(10,1,5,'NUEVO SERVICIO DE PRUEBA',100,'uploads/catalogo/Naruto-Shippuden-Pain.jpg',1,7);
/*!40000 ALTER TABLE `servicio_empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submenu`
--

DROP TABLE IF EXISTS `submenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `submenu` (
  `idSubmenu` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `icono` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT '1',
  `menuIdMenu` int(11) DEFAULT NULL,
  PRIMARY KEY (`idSubmenu`),
  KEY `FK_acaff8a802d765dc6b657cc20fd` (`menuIdMenu`),
  CONSTRAINT `FK_acaff8a802d765dc6b657cc20fd` FOREIGN KEY (`menuIdMenu`) REFERENCES `menu` (`idMenu`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submenu`
--

LOCK TABLES `submenu` WRITE;
/*!40000 ALTER TABLE `submenu` DISABLE KEYS */;
INSERT INTO `submenu` VALUES (1,'Personas','fa fa-user','blue','/maestros/persona',1,1);
/*!40000 ALTER TABLE `submenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(255) NOT NULL,
  `clave` varchar(255) NOT NULL,
  `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `rolIdRol` int(11) DEFAULT NULL,
  `personaId` int(11) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idUsuario`),
  UNIQUE KEY `IDX_9921cd8ed63a072b8f93ead80f` (`usuario`),
  UNIQUE KEY `REL_aa5c122ee9bc3ce4daf6af75dd` (`personaId`),
  KEY `FK_eb1420806973a412e1145c27e61` (`rolIdRol`),
  CONSTRAINT `FK_aa5c122ee9bc3ce4daf6af75dde` FOREIGN KEY (`personaId`) REFERENCES `persona` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_eb1420806973a412e1145c27e61` FOREIGN KEY (`rolIdRol`) REFERENCES `rol` (`idRol`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'admin','admin','2020-09-15 00:00:00.000000','2022-03-15 22:53:13.000000',1,1,'uploads/manuales/CRISTIANO_550x650.jpg'),(2,'richardQC','123','2020-09-16 20:51:19.849801','2022-03-16 07:59:52.000000',2,4,'uploads/manuales/CRISTIANO_550x650.jpg'),(3,'brandonAC','321','2020-09-16 21:21:47.932850','2022-01-24 22:30:22.000000',3,3,NULL),(4,'prueba','prueba','2022-01-19 22:56:35.999392','2022-01-25 22:30:05.000000',2,6,NULL),(5,'nuevo','nuevo','2022-03-16 08:03:09.651521','2022-03-16 08:10:19.000000',2,7,'uploads/manuales/2.png');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'pelucacool'
--
/*!50003 DROP PROCEDURE IF EXISTS `sp_cliente_crud` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cliente_crud`(
    in xopcion int ,
    in xidpersona int,
    in xidcliente int,
    in xnombre varchar(150),
    in xapellido varchar(150),
    in xdni varchar(8),
    in xfechaNacimiento varchar(100),
    in xcorreo varchar(120),
    in xtipoPersona varchar(100),
    in xtelefono varchar(120),
    in xestado int,
    in xusuario varchar(100),
    in xclave varchar(100),
    in xrol int
)
begin
    declare ultimoRegistrado int;
    declare Idinsert int;
    -- buscar por id
    if xopcion=1 then
        select c.idCliente,c.estado,
               json_object('id',p2.id,'nombre',p2.nombre,'apellido',p2.apellido,'dni',p2.dni,
                           'fechaNacimiento',p2.fechaNacimiento,'correo',p2.correo,'celular',p2.celular,
                           'estado',p2.estado,'tipo',p2.tipo) as persona
        from cliente c left join persona p2 on p2.id = c.personaId

        where c.idCliente=xidcliente;
    end if;

    -- insertar cliente
    if xopcion=2 then
        insert into persona(nombre, apellido, fechaNacimiento, correo, tipo, celular, dni)
        values (xnombre,xapellido,xfechaNacimiento,xcorreo,xtipoPersona,xtelefono,xdni);
        set ultimoRegistrado=last_insert_id() ;
        insert into cliente(personaId)values (ultimoRegistrado);
        set Idinsert=last_insert_id ();
        insert into usuario( usuario, clave,  rolIdRol, personaId)
        values (xusuario,xclave,xrol,ultimoRegistrado);


        select c.idCliente,
               json_object(
                   'id',p.id,
                   'nombre',p.nombre,
                   'apellido',p.apellido,
                   'fechaNacimiento',p.fechaNacimiento,
                   'tipo',p.tipo,
                   'celular',p.celular,
                   'dni',p.dni,
                   'correo',p.correo,
                   'estado',p.estado,
                   'createdAt',p.createdAt) as persona,
        json_object('idUsuario',u.idUsuario,'clave',u.clave,'idRol',u.rolIdRol,'usuario',u.usuario) as usuario
        from cliente c
                 left join persona p on p.id = c.personaId
        left join usuario u on p.id = u.personaId
        where c.idCliente=Idinsert;

    end if;

    -- eliminar
    if xopcion=3 then
        delete from cliente where idCliente=xidcliente;
        delete from persona where id=xidpersona;
    end if;

    if xopcion=4 then
        select c.idCliente,c.estado,c.personaId,
               json_object('id',p2.id,'nombre',p2.nombre,'apellido',p2.apellido,'dni',p2.dni,
                           'fechaNacimiento',p2.fechaNacimiento,'correo',p2.correo,'celular',p2.celular,
                           'estado',p2.estado,'tipo',p2.tipo) as persona
        from cliente c left join persona p2 on p2.id = c.personaId;

    end if;

    -- actualizacion de emplead
    if xopcion=5 then
        update persona pe set pe.nombre=xnombre,pe.apellido=xapellido,
                              pe.correo=xcorreo,
                              pe.celular=xtelefono,
                              pe.dni=xdni,
                              pe.tipo=xtipoPersona,
                              pe.fechaNacimiento=xfechaNacimiento
        where pe.id=xidpersona;
    end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_empleado_crud` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_empleado_crud`(
    in xopcion int ,
    in xidpersona int,
    in xidEmpleado int,
    in xnombre varchar(150),
    in xapellido varchar(150),
    in xdni varchar(8),
    in xfechaNacimiento varchar(100),
    in xcorreo varchar(120),
    in xtipoPersona varchar(100),
    in xtelefono varchar(120),
    in xtipoEmpleado varchar(100),
    in xEmpresaId int
)
begin
    declare ultimoRegistrado int;
    declare propietarioIdinsert int;
    -- buscar por id
    if xopcion=1 then
        select em.idEmpleado,em.areaId,em.empresaId,em.personaId,em.tipoEmpleado,
               JSON_OBJECT('id',e.id,'correo',e.correo,'direccion',e.direccion,
                           'razonSocial',e.razonSocial,'ruc',e.ruc,'telefono',e.telefono,
                           'tipoNegocio',e.tipoNegocio,'propietarioId',e.propietarioId) AS empresa,
               json_object('id',p2.id,'nombre',p2.nombre,'apellido',p2.apellido,'dni',p2.dni,
                           'fechaNacimiento',p2.fechaNacimiento,'correo',p2.correo,'celular',p2.celular,
                           'estado',p2.estado,'tipo',p2.tipo) as persona
        from empleado em left join persona p2 on p2.id = em.personaId
                         left join empresa e on e.id = em.empresaId
        where em.idEmpleado=xidEmpleado;
    end if;

    -- insertar empleado
    if xopcion=2 then
        insert into persona(nombre, apellido, fechaNacimiento, correo, tipo, celular, dni)
        values (xnombre,xapellido,xfechaNacimiento,xcorreo,xtipoPersona,xtelefono,xdni);
        set ultimoRegistrado=last_insert_id() ;
        insert into empleado(personaId,tipoEmpleado,empresaId)values (ultimoRegistrado,xtipoEmpleado,xEmpresaId);
        set propietarioIdinsert=last_insert_id ();
        select pro.idEmpleado,pro.tipoEmpleado,json_object('id',p.id,'nombre',p.nombre,'apellido',p.apellido,'fechaNacimiento',p.fechaNacimiento,
                                                    'tipo',p.tipo,'celular',p.celular,'dni',p.dni,'correo',p.correo,'estado',p.estado,'createdAt',p.createdAt) as persona
        from empleado pro
                 left join persona p on p.id = pro.personaId
        where pro.idEmpleado=propietarioIdinsert;

    end if;

    -- eliminar
    if xopcion=3 then
        delete from empleado where idEmpleado=xidEmpleado;
        delete from persona where id=xidpersona;
    end if;

    if xopcion=4 then
        select em.idEmpleado,em.areaId,em.empresaId,em.personaId,em.tipoEmpleado,
               JSON_OBJECT('id',e.id,'correo',e.correo,'direccion',e.direccion,
                   'razonSocial',e.razonSocial,'ruc',e.ruc,'telefono',e.telefono,
                   'tipoNegocio',e.tipoNegocio,'propietarioId',e.propietarioId) AS empresa,
               json_object('id',p2.id,'nombre',p2.nombre,'apellido',p2.apellido,'dni',p2.dni,
                   'fechaNacimiento',p2.fechaNacimiento,'correo',p2.correo,'celular',p2.celular,
                   'estado',p2.estado,'tipo',p2.tipo) as persona
        from empleado em left join persona p2 on p2.id = em.personaId
        left join empresa e on e.id = em.empresaId;
    end if;

    -- actualizacion de emplead
    if xopcion=5 then
        update empleado em set  em.tipoEmpleado=xtipoEmpleado,em.empresaId=xEmpresaId
        where em.idEmpleado=xidEmpleado;
        update persona pe set pe.nombre=xnombre,pe.apellido=xapellido,
            pe.correo=xcorreo,
            pe.celular=xtelefono,
            pe.dni=xdni,
            pe.tipo=xtipoPersona,
            pe.fechaNacimiento=xfechaNacimiento
        where pe.id=xidpersona;
    end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_empresas_crud` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_empresas_crud`(
in xopcion int,
in xidx int,
in xruc varchar(11),
in xrazonSocial varchar(100),
in xdireccion varchar(150),
in xtelefono varchar(14),
in xcorreo varchar(100),
in xtipoNegocio int,
in xpropietarioId int

)
begin
declare ultimoInsertado int;
-- listar toda la informacion de la tabla empresas 
-- con sus propietarios y sacanso su informacion de la tabla persona

if xopcion=1 
	then
     select emp.id, emp.razonSocial,emp.ruc,emp.direccion,emp.telefono,emp.correo,emp.tipoNegocio,emp.propietarioId,
     pro.tipoComercial,personaId,per.nombre,per.apellido,per.fechaNacimiento,
     per.correo as personaCorreo,per.tipo,per.celular,per.dni,per.estado from empresa emp
     left join propietario pro on emp.propietarioId=pro.id
     left join persona per on pro.personaId=per.id
     order by emp.id asc
     ;
    end if;
    
 -- buscar por ruc   
if xopcion=2
	then
     select *from empresa emp where emp.ruc=xruc;
    end if;
-- buscarpor ID
if xopcion=3 then 
	select *from empresa emp where emp.id=xidx;
end if;
-- eliminar por id
if xopcion=4 then 
	delete from empresa where id=xidx;
end if;
    
    if xopcion=5
	then
     insert into empresa(razonSocial,ruc,direccion,telefono,correo,propietarioId)
     values(xrazonSocial,xruc,xdireccion,xtelefono,xcorreo,xpropietarioId);
     set ultimoInsertado =last_insert_id();
     select *from empresa empr where empr.id=ultimoInsertado;
     
    end if;
    
    if xopcion=6
	then
     update empresa set razonSocial=xrazonSocial,
     ruc=xruc,
     direccion=xdireccion,
     telefono=xtelefono,
     correo=xcorreo,
     propietarioId=xpropietarioId
     where id=xidx;
    
     
    end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_propietario_crud` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_propietario_crud`(
    in xopcion int ,
    in xidpersona int,
    in xidpropietario int,
    in xnombre varchar(150),
    in xapellido varchar(150),
    in xdni varchar(8),
    in xfechaNacimiento varchar(100),
    in xcorreo varchar(120),
    in xtipoPersona varchar(100),
    in xtelefono varchar(120),
    in xtipoComercial varchar(100)
    )
begin
    declare ultimoRegistrado int;
    declare propietarioIdinsert int;
    -- actualizar informacion de propietarios
    if xopcion=1 then
        update propietario pro set pro.tipoComercial=xtipoComercial
        where pro.id=xidpropietario;

        update persona pe set pe.nombre=xnombre,pe.apellido=xapellido,
                              pe.correo=xcorreo,
                              pe.celular=xtelefono,
                              pe.dni=xdni,
                              pe.tipo=xtipoPersona,
                              pe.fechaNacimiento=xfechaNacimiento
        where pe.id=xidpersona;
    end if;
    if xopcion=2 then
        insert into persona(nombre, apellido, fechaNacimiento, correo, tipo, celular, dni)
        values (xnombre,xapellido,xfechaNacimiento,xcorreo,xtipoPersona,xtelefono,xdni);
        set ultimoRegistrado=last_insert_id() ;
        insert into propietario(personaId)values (ultimoRegistrado);
        set propietarioIdinsert=last_insert_id ();
        select pro.id,pro.tipoComercial,json_object('id',p.id,'nombre',p.nombre,'apellido',p.apellido,'fechaNacimiento',p.fechaNacimiento,
                       'tipo',p.tipo,'celular',p.celular,'dni',p.dni,'correo',p.correo,'estado',p.estado,'createdAt',p.createdAt) as persona
        from propietario pro
            left join persona p on p.id = pro.personaId
        where pro.id=propietarioIdinsert;

    end if;

    if xopcion=3 then
        delete from propietario where id=xidpropietario;
        delete from persona where id=xidpersona;
    end if;

end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_usuario_crud` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_usuario_crud`(
in xopcion int ,
in xid int,
in xusuario varchar(150),
in xclave varchar(150))
begin
    declare ultimoRegistrado int;
    -- lista de usuarios
if xopcion=1 then
    select
           u.idUsuario,
        u.usuario ,
        u.clave,
        json_object(
                'id', p.id,
                'nombre', p.nombre,
                'apellido', p.apellido,
                'celular', p.celular,
                'correo', p.correo,
                'dni', p.dni,
                'fechaNacimiento', p.fechaNacimiento,
                'tipo', p.tipo,
                'estado', p.estado)
              as persona,

        json_object(
                'nombre',r.nombre,
                'idRol',r.idRol
            ) as rol

    from usuario u left join persona p on p.id = u.personaId
                   left join rol r on r.idRol = u.rolIdRol;
end if;

    -- busqueda por id
    if xopcion=2 then
        select u.idusuario, usuario, clave, rolidrol, personaid,
               json_object('nombre',r.nombre,'idRol',r.idRol,'estado',r.estado) as rol,
               json_object('id',p.id,'nombre',p.nombre,'apellido',p.apellido,
                   'estado',p.estado,'fechaNacimiento',p.fechaNacimiento,
                   'dni',p.dni,'correo',p.correo,'celular',p.celular,
                   'tipo',p.tipo) as persona
        from usuario u
        left join rol r on r.idRol = u.rolIdRol
        left join persona p on p.id = u.personaId
        where u.idUsuario=xid;

        end if;

    -- busqueda por clave y usuario
    if xopcion=3 then
        select u.idusuario, usuario, clave, rolidrol, personaid,
               json_object('nombre',r.nombre,'idRol',r.idRol,'estado',r.estado) as rol
        from usuario u                                                                                                left join rol r on r.idRol = u.rolIdRol
        where u.usuario=xusuario and u.clave=xclave;

    end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-20 22:24:32
